package com.example._12_pvh_sunsythorng_hw003.exception;

public class NotFoundException extends RuntimeException{
    String title;
    public NotFoundException(String title, String message) {
        super(message);
        this.title=title;
    }

    public String getTitle() {
        return title;
    }
}
