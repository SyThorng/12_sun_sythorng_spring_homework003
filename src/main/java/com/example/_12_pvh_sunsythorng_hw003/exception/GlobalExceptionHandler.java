package com.example._12_pvh_sunsythorng_hw003.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.net.URI;


@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    ProblemDetail FieldNotFound(
            NotFoundException authorNotFoundException
    ){
        ProblemDetail problemDetail=ProblemDetail.forStatusAndDetail(
                HttpStatus.NOT_FOUND,authorNotFoundException.getMessage()
        );
        problemDetail.setType(URI.create("localhost:8080/error/not-found"));
        problemDetail.setTitle(authorNotFoundException.getTitle()+"");
       return problemDetail;
    }

    @ExceptionHandler(EmptyFieldExcaption.class)
    ProblemDetail EmptyField(
            EmptyFieldExcaption emptyFieldAuthorExcaption
    ){
        ProblemDetail problemDetail=ProblemDetail.forStatusAndDetail(
                HttpStatus.BAD_REQUEST,emptyFieldAuthorExcaption.getMessage()
        );
        problemDetail.setTitle("invalid input..!!!");
        problemDetail.setType(URI.create("localhost:8080/error/not-found"));
        return problemDetail;
    }
}
