package com.example._12_pvh_sunsythorng_hw003.exception;

public class EmptyFieldExcaption extends RuntimeException{
    public EmptyFieldExcaption(String message) {
        super(message);
    }
}
