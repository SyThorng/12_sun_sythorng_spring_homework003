package com.example._12_pvh_sunsythorng_hw003.service;

import com.example._12_pvh_sunsythorng_hw003.model.Category;
import com.example._12_pvh_sunsythorng_hw003.model.request.CategoryRequest;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategory();
    Category getAllCategoryById(Integer id);

    Category deleteCategory(Integer id);

    Category insertCategory(CategoryRequest categoryRequest);
    Category updateCategory(CategoryRequest categoryRequest,Integer id);
}
