package com.example._12_pvh_sunsythorng_hw003.service;


import com.example._12_pvh_sunsythorng_hw003.model.Authors;
import com.example._12_pvh_sunsythorng_hw003.model.request.AuthorsRequest;

import java.util.List;

public interface AuthorService {
    List<Authors> getAllAuthor();
    Authors getAllAuthorById(Integer id);

    Authors deleteAuthor(Integer id);

    Authors insertAuthor(AuthorsRequest authorsRequest);
    Authors updateAuthor(AuthorsRequest authorsRequest, Integer id);
}
