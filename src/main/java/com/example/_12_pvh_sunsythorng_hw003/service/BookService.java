package com.example._12_pvh_sunsythorng_hw003.service;



import com.example._12_pvh_sunsythorng_hw003.model.Book;
import com.example._12_pvh_sunsythorng_hw003.model.request.BookRequest;

import java.util.List;

public interface BookService {
    List<Book> getAllBook();
    Book searchBookById(Integer id);
    Book deleteBookById(Integer id);

    Integer insertBook(BookRequest bookRequest);

    Integer updateBook(Integer id,BookRequest bookRequest);

}
