package com.example._12_pvh_sunsythorng_hw003.service.impl;

import com.example._12_pvh_sunsythorng_hw003.exception.NotFoundException;
import com.example._12_pvh_sunsythorng_hw003.exception.EmptyFieldExcaption;
import com.example._12_pvh_sunsythorng_hw003.model.Authors;
import com.example._12_pvh_sunsythorng_hw003.model.constant.Gender;
import com.example._12_pvh_sunsythorng_hw003.repository.AuthorRepository;
import com.example._12_pvh_sunsythorng_hw003.model.request.AuthorsRequest;
import com.example._12_pvh_sunsythorng_hw003.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorImpl implements AuthorService {
    private final AuthorRepository authorRepository;

    public AuthorImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Authors> getAllAuthor() {
        return authorRepository.getAllAuthor();
    }

    @Override
    public Authors getAllAuthorById(Integer id) {
        Authors authors = authorRepository.getAllAuthorById(id);
        if (authors == null) {
            throw new NotFoundException("Authors not Found","Authors with id " + id + " not found");
        }
        return authorRepository.getAllAuthorById(id);
    }

    @Override
    public Authors deleteAuthor(Integer id) {
        Authors authors = authorRepository.getAllAuthorById(id);
        if (authors == null) {
            throw new NotFoundException("Authors not Found","Authors with id " + id + " not found");
        }
        return authorRepository.deleteAuthor(id);
    }

    @Override
    public Authors insertAuthor(AuthorsRequest authorsRequest) {

        if(authorsRequest.getAuthorName()==null){
            throw new EmptyFieldExcaption("Field cannot null..!");
        }
        else if (authorsRequest.getAuthorName().equals("string") && authorsRequest.getGender().equals("string")) {
            throw new EmptyFieldExcaption("Please input Name & Gender");
        } else if ((authorsRequest.getAuthorName().isEmpty() && authorsRequest.getGender().isEmpty()) || (authorsRequest.getAuthorName().matches("\\s+"))) {
            throw new EmptyFieldExcaption("Cannot Empty Name & Gender");
        } else if (authorsRequest.getAuthorName().isEmpty()) {
            throw new EmptyFieldExcaption("Cannot Empty Name & Gender");
        } else if (authorsRequest.getGender().isEmpty()) {
            throw new EmptyFieldExcaption("Gender cannot empty...!!");
        } else {
            boolean b=false;
            for (Gender gender: Gender.values()
                 ) {
                if(authorsRequest.getGender().equalsIgnoreCase(gender.name())){
                    b=true;
                    break;
                }
            }
            if(!b){
                throw new EmptyFieldExcaption("invalid gender..!!");
            }
            authorsRequest.setAuthorName(authorsRequest.getAuthorName().trim());
            authorsRequest.setGender(authorsRequest.getGender().toLowerCase());
            return authorRepository.insertAuthor(authorsRequest);
        }
    }

    @Override
    public Authors updateAuthor(AuthorsRequest authorsRequest, Integer id) {
        Authors author = authorRepository.getAllAuthorById(id);
        if (author == null) {
            throw new NotFoundException("Authors not Found","Authors with id " + id + " not found");
        } else if (authorsRequest.getAuthorName().equals("string") && authorsRequest.getGender().equals("string")) {
            throw new EmptyFieldExcaption("Please input Name & Gender");
        } else if (authorsRequest.getAuthorName().isEmpty() && authorsRequest.getGender().isEmpty() || authorsRequest.getAuthorName().isBlank()) {
            throw new EmptyFieldExcaption("Cannot Empty Name & Gender");
        } else if (authorsRequest.getAuthorName().isEmpty()) {
            throw new EmptyFieldExcaption("Cannot Empty Name & Gender");
        } else if (authorsRequest.getGender().isEmpty()) {
            throw new EmptyFieldExcaption("Gender cannot empty...!!");
        }  else {
            boolean b = false;
            for (Gender gender : Gender.values()
            ) {
                if (authorsRequest.getGender().equalsIgnoreCase(gender.name())) {
                    b = true;
                    break;
                }
            }
            if (!b) {
                throw new EmptyFieldExcaption("invalid gender..!!");
            }
        }

        authorsRequest.setAuthorName(authorsRequest.getAuthorName().trim());
        authorsRequest.setGender(authorsRequest.getGender().toLowerCase());
        return authorRepository.updateAuthor(authorsRequest, id);
    }

}

