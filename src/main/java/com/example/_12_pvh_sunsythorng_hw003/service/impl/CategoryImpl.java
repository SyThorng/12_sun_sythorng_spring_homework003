package com.example._12_pvh_sunsythorng_hw003.service.impl;

import com.example._12_pvh_sunsythorng_hw003.exception.NotFoundException;
import com.example._12_pvh_sunsythorng_hw003.exception.EmptyFieldExcaption;
import com.example._12_pvh_sunsythorng_hw003.model.Category;
import com.example._12_pvh_sunsythorng_hw003.model.request.CategoryRequest;
import com.example._12_pvh_sunsythorng_hw003.repository.CategoryRepository;
import com.example._12_pvh_sunsythorng_hw003.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CategoryImpl implements CategoryService {


    private final CategoryRepository categoryRepository;


    public CategoryImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategory() {
        return categoryRepository.getAllCategory();
    }

    @Override
    public Category getAllCategoryById(Integer id) {
        if (categoryRepository.getAllCategoryById(id) == null) {
            throw new NotFoundException("Category not Found...!!", "Category with id " + id + " not found");
        }
        return categoryRepository.getAllCategoryById(id);
    }

    @Override
    public Category deleteCategory(Integer id) {
        if (categoryRepository.getAllCategoryById(id) == null) {
            throw new NotFoundException("Category not Found...!!", "Category with id " + id + " not found");
        }
        return categoryRepository.deleteCategory(id);
    }

    @Override
    public Category insertCategory(CategoryRequest categoryRequest) {
        if(categoryRequest.getCategoryName()==null){
            throw new EmptyFieldExcaption("CategoryName Cannot null");
        }
        else if (categoryRequest.getCategoryName().equals("string")) {
            throw new EmptyFieldExcaption("Please input CategoryName");
        } else if (categoryRequest.getCategoryName().isEmpty() || categoryRequest.getCategoryName().isBlank()) {
            throw new EmptyFieldExcaption("Cannot Empty CategoryName");
        }
        categoryRequest.setCategoryName(categoryRequest.getCategoryName().trim());
        return categoryRepository.insertCategory(categoryRequest);
    }

    @Override
    public Category updateCategory(CategoryRequest categoryRequest, Integer id) {
        if (categoryRepository.getAllCategoryById(id) == null) {
            throw new NotFoundException("Category not Found...!!", "Category with id " + id + " not found");
        } else {
            if(categoryRequest.getCategoryName()==null){
                throw new EmptyFieldExcaption("CategoryName Cannot null");
            }
            else if (categoryRequest.getCategoryName().equals("string")) {
                throw new EmptyFieldExcaption("Please input CategoryName");
            } else if (categoryRequest.getCategoryName().isEmpty() || categoryRequest.getCategoryName().isBlank()) {
                throw new EmptyFieldExcaption("Cannot Empty CategoryName");
            }
        }
        categoryRequest.setCategoryName(categoryRequest.getCategoryName().trim());
        return categoryRepository.updateCategory(categoryRequest, id);
    }
}
