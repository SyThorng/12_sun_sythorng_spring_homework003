package com.example._12_pvh_sunsythorng_hw003.service.impl;

import com.example._12_pvh_sunsythorng_hw003.exception.NotFoundException;
import com.example._12_pvh_sunsythorng_hw003.exception.EmptyFieldExcaption;
import com.example._12_pvh_sunsythorng_hw003.model.Book;
import com.example._12_pvh_sunsythorng_hw003.model.request.BookRequest;
import com.example._12_pvh_sunsythorng_hw003.repository.BookRepository;
import com.example._12_pvh_sunsythorng_hw003.service.AuthorService;
import com.example._12_pvh_sunsythorng_hw003.service.BookService;
import com.example._12_pvh_sunsythorng_hw003.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookImpl implements BookService {
    private final BookRepository repository;

    private final AuthorService authorService;
    private final CategoryService categoryService;

    public BookImpl(BookRepository repository, AuthorService authorService, CategoryService categoryService) {
        this.repository = repository;
        this.authorService = authorService;
        this.categoryService = categoryService;
    }


    @Override
    public List<Book> getAllBook() {
        return repository.getAllBook();
    }

    @Override
    public Book searchBookById(Integer id) {
        if (repository.searchBookById(id) == null) {
            throw new NotFoundException("Book not Found..!!", "Book with id " + id + " not found");
        }
        return repository.searchBookById(id);
    }

    @Override
    public Book deleteBookById(Integer id) {
        if (repository.searchBookById(id) == null) {
            throw new NotFoundException("Book not Found..!!", "Book with id " + id + " not found");
        }
        return repository.deleteBookById(id);
    }

    @Override
    public Integer insertBook(BookRequest bookRequest) {

        if (bookRequest.getTitle() == null) {
            throw new EmptyFieldExcaption("title cannot empty..!!");
        } else if (bookRequest.getTitle().equals("string")) {
            throw new EmptyFieldExcaption("Please input title");
        } else if (bookRequest.getTitle().isEmpty() || bookRequest.getTitle().isBlank()) {
            throw new EmptyFieldExcaption("Cannot Empty title");
        }
        authorService.getAllAuthorById(bookRequest.getAuthors());

        for (int i = 0; i < bookRequest.getCategories().size(); i++) {
            categoryService.getAllCategoryById(bookRequest.getCategories().get(i));
        }

        bookRequest.setTitle(bookRequest.getTitle().trim());
        Integer id = repository.insertBook(bookRequest);
        for (int i = 0; i < bookRequest.getCategories().size(); i++) {
            repository.addBookDetails(id, bookRequest.getCategories().get(i));
        }
        return id;
    }

    @Override
    public Integer updateBook(Integer id, BookRequest bookRequest) {
        if (repository.searchBookById(id) == null) {
            throw new NotFoundException("Book not Found..!!", "Book with id " + id + " not found");
        } else {
            if (bookRequest.getTitle() == null) {
                throw new EmptyFieldExcaption("title cannot empty..!!");
            } else if (bookRequest.getTitle().equals("string")) {
                throw new EmptyFieldExcaption("Please input title");
            } else if (bookRequest.getTitle().isEmpty() || bookRequest.getTitle().isBlank()) {
                throw new EmptyFieldExcaption("Cannot Empty title");
            }
            authorService.getAllAuthorById(bookRequest.getAuthors());
            for (int i = 0; i < bookRequest.getCategories().size(); i++) {
                categoryService.getAllCategoryById(bookRequest.getCategories().get(i));
            }
            bookRequest.setTitle(bookRequest.getTitle().trim());
            int book_id = repository.updateBook(id, bookRequest);
            repository.deleteBookforUpdate(book_id);
            for (int i = 0; i < bookRequest.getCategories().size(); i++) {
                repository.addBookDetails(book_id, bookRequest.getCategories().get(i));
            }
            return book_id;
        }
    }
}
