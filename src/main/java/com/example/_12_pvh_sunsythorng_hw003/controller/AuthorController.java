package com.example._12_pvh_sunsythorng_hw003.controller;

import com.example._12_pvh_sunsythorng_hw003.exception.EmptyFieldExcaption;
import com.example._12_pvh_sunsythorng_hw003.model.Authors;
import com.example._12_pvh_sunsythorng_hw003.model.request.AuthorsRequest;
import com.example._12_pvh_sunsythorng_hw003.model.respon.Respon;
import com.example._12_pvh_sunsythorng_hw003.service.AuthorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("api/v1/authors")
public class AuthorController {
    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("/getAllAuthors")
    public ResponseEntity<Respon<List<Authors>>> getAllAuthor() {
            if(authorService.getAllAuthor()==null){
                throw new EmptyFieldExcaption("No Have Author..!!");
            }
        Respon<List<Authors>> respon = Respon.<List<Authors>>builder()
                .timestamp(new Timestamp(new Date().getTime()))
                .status(200)
                .message("Successfully fetched authors")
                .payload(authorService.getAllAuthor()).build();
        return ResponseEntity.ok().body(respon);
    }

    @GetMapping("/GetAuthorById/{id}")
    public ResponseEntity<Respon<Authors>> getAllAuthorById(
            @PathVariable("id") int id
    ) {
        if (authorService.getAllAuthorById(id) != null) {
            Respon<Authors> respon = Respon.<Authors>builder()
                    .timestamp(new Timestamp(new Date().getTime()))
                    .status(200)
                    .message("Successfully fetched authors")
                    .payload(authorService.getAllAuthorById(id))
                    .build();
            return ResponseEntity.ok().body(respon);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/deleteAuthor/{id}")
    public ResponseEntity<Respon<Authors>> deleteAuthor(
            @PathVariable("id") int id
    ) {
        if (authorService.getAllAuthorById(id) != null) {
            Respon<Authors> respon = Respon.<Authors>builder()
                    .timestamp(new Timestamp(new Date().getTime()))
                    .status(200)
                    .message("Successfully deleted author")
                    .payload(authorService.deleteAuthor(id))
                    .build();
            return ResponseEntity.ok().body(respon);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/insertAuthor/")
    public ResponseEntity<Respon<Authors>> insertAuthor(
            @RequestBody AuthorsRequest authorsRequest
    ) {
        Respon<Authors> respon = Respon.<Authors>builder()
                .timestamp(new Timestamp(new Date().getTime()))
                .status(200)
                .message("Successfully added author")
                .payload(authorService.insertAuthor(authorsRequest))
                .build();
        return ResponseEntity.ok().body(respon);
    }

    @PutMapping("/updateAuthor/{id}")
    public ResponseEntity<Respon<Authors>> updateAuthor(
            @PathVariable("id") int id, @RequestBody AuthorsRequest authorsRequest
    ) {
        if (authorService.getAllAuthorById(id) != null) {
            Respon<Authors> respon = Respon.<Authors>builder()
                    .timestamp(new Timestamp(new Date().getTime()))
                    .status(200)
                    .message("Successfully deleted author")
                    .payload(authorService.updateAuthor(authorsRequest, id))
                    .build();
            return ResponseEntity.ok().body(respon);
        }
        return ResponseEntity.notFound().build();
    }
}
