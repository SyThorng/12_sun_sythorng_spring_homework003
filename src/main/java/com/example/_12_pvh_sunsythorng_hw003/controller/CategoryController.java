package com.example._12_pvh_sunsythorng_hw003.controller;

import com.example._12_pvh_sunsythorng_hw003.exception.EmptyFieldExcaption;
import com.example._12_pvh_sunsythorng_hw003.model.Category;
import com.example._12_pvh_sunsythorng_hw003.model.request.CategoryRequest;
import com.example._12_pvh_sunsythorng_hw003.model.respon.Respon;
import com.example._12_pvh_sunsythorng_hw003.service.CategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("api/v1/Category")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @GetMapping("/getAllCategory")
    public ResponseEntity<Respon<List<Category>>> getAllCategory() {
        if(categoryService.getAllCategory()==null){
            throw new EmptyFieldExcaption("No have Category..!!");
        }
        Respon<List<Category>> respon = Respon.<List<Category>>builder()
                .timestamp(new Timestamp(new Date().getTime()))
                .status(200)
                .message("Successfully fetched categories")
                .payload(categoryService.getAllCategory()).build();
        return ResponseEntity.ok().body(respon);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Respon<Category>> getAllCategoryById(
            @PathVariable("id") int id
    ) {
        if (categoryService.getAllCategoryById(id) != null) {
            Respon<Category> respon = Respon.<Category>builder()
                    .timestamp(new Timestamp(new Date().getTime()))
                    .status(200)
                    .message("Successfully fetched categories")
                    .payload(categoryService.getAllCategoryById(id))
                    .build();
            return ResponseEntity.ok().body(respon);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Respon<Category>> deleteCategory(
            @PathVariable("id") int id
    ) {
        if (categoryService.getAllCategoryById(id) != null) {
            Respon<Category> respon = Respon.<Category>builder()
                    .timestamp(new Timestamp(new Date().getTime()))
                    .status(200)
                    .message("Successfully deleted categories")
                    .payload(categoryService.deleteCategory(id))
                    .build();
            return ResponseEntity.ok().body(respon);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/")
    public ResponseEntity<Respon<Category>> insertAuthor(
            @RequestBody CategoryRequest categoryRequest
            ) {
        Respon<Category> respon = Respon.<Category>builder()
                .timestamp(new Timestamp(new Date().getTime()))
                .status(200)
                .message("Successfully added categories")
                .payload(categoryService.insertCategory(categoryRequest))
                .build();
        return ResponseEntity.ok().body(respon);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Respon<Category>> updateAuthor(
            @PathVariable("id") int id,
            @RequestBody CategoryRequest categoryRequest
    ) {
        if (categoryService.getAllCategoryById(id) != null) {
            Respon<Category> respon = Respon.<Category>builder()
                    .timestamp(new Timestamp(new Date().getTime()))
                    .status(200)
                    .message("Successfully deleted author")
                    .payload(categoryService.updateCategory(categoryRequest,id))
                    .build();
            return ResponseEntity.ok().body(respon);
        }
        return ResponseEntity.notFound().build();
    }

}
