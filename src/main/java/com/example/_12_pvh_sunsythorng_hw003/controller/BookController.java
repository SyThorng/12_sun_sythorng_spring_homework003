package com.example._12_pvh_sunsythorng_hw003.controller;

import com.example._12_pvh_sunsythorng_hw003.exception.EmptyFieldExcaption;
import com.example._12_pvh_sunsythorng_hw003.exception.NotFoundException;
import com.example._12_pvh_sunsythorng_hw003.model.Authors;
import com.example._12_pvh_sunsythorng_hw003.model.Book;
import com.example._12_pvh_sunsythorng_hw003.model.request.BookRequest;
import com.example._12_pvh_sunsythorng_hw003.model.respon.Respon;
import com.example._12_pvh_sunsythorng_hw003.repository.BookRepository;
import com.example._12_pvh_sunsythorng_hw003.service.BookService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("api/vi/books")
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/getAllBooks")
    public ResponseEntity<Respon<List<Book>>> getAllBook() {
        if (bookService.getAllBook()==null){
            throw  new EmptyFieldExcaption("Not have Book");
        }
        Respon<List<Book>> respon = Respon.<List<Book>>builder()
                .timestamp(new Timestamp(new Date().getTime()))
                .status(200)
                .message("Successfully fetched books")
                .payload(bookService.getAllBook()).build();
        return ResponseEntity.ok().body(respon);
    }

    @GetMapping("/searchBookById/{id}")
    public ResponseEntity<Respon<Book>> searchBookById(
            @PathVariable("id") int id
    ) {
        if (bookService.searchBookById(id) != null) {
            Respon<Book> respon = Respon.<Book>builder()
                    .timestamp(new Timestamp(new Date().getTime()))
                    .status(200)
                    .message("Successfully fetched Book")
                    .payload(bookService.searchBookById(id))
                    .build();
            return ResponseEntity.ok().body(respon);
        }
        return ResponseEntity.notFound().build();
    }


    @DeleteMapping("/deleteBookById/{id}")
    public ResponseEntity<Respon<Book>> deleteBookById(
            @PathVariable("id") int id
    ) {
        if (bookService.searchBookById(id) != null) {
            Respon<Book> respon = Respon.<Book>builder()
                    .timestamp(new Timestamp(new Date().getTime()))
                    .status(200)
                    .message("Successfully deleted book")
                    .payload(bookService.deleteBookById(id))
                    .build();
            return ResponseEntity.ok().body(respon);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/insertBook")
    public ResponseEntity<Respon<Book>> insertBook(
            @RequestBody BookRequest bookRequest
            ) {
            int id= bookService.insertBook(bookRequest);
            Book book=bookService.searchBookById(id);
            Respon<Book> respon = Respon.<Book>builder()
                    .timestamp(new Timestamp(new Date().getTime()))
                    .status(200)
                    .message("Successfully added Book")
                    .payload(book)
                    .build();
            return ResponseEntity.ok().body(respon);
    }

    @PutMapping("/UpdateBook/{id}")
    public ResponseEntity<Respon<Book>> UpdateBook(
            @RequestBody BookRequest bookRequest,@PathVariable int id
    ) {
        int book_id= bookService.updateBook(id,bookRequest);
        Book book=bookService.searchBookById(book_id);
        Respon<Book> respon = Respon.<Book>builder()
                .timestamp(new Timestamp(new Date().getTime()))
                .status(200)
                .message("Successfully added Book")
                .payload(book)
                .build();
        return ResponseEntity.ok().body(respon);
    }
}
