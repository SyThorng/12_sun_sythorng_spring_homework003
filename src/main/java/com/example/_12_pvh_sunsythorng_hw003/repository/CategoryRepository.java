package com.example._12_pvh_sunsythorng_hw003.repository;

import com.example._12_pvh_sunsythorng_hw003.model.Category;
import com.example._12_pvh_sunsythorng_hw003.model.request.CategoryRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {
    @Select("Select * from categories_tb")
    @Results(id = "mapCategory", value = {
            @Result(property = "categoryId", column = "category_id"),
            @Result(property = "categoryName", column = "category_name")
    })
    List<Category> getAllCategory();


    @Select("""
            Select * from categories_tb where category_id=#{id}
            """)
    @ResultMap("mapCategory")
    Category getAllCategoryById(Integer id);


    @Select("""
            DELETE  FROM categories_tb where category_id=#{id}
             """)
    @ResultMap("mapCategory")
    Category deleteCategory(Integer id);

    @Select("""
            insert into categories_tb (category_name) values (#{cate.categoryName}) returning *
            """)
    @ResultMap("mapCategory")
    Category insertCategory(@Param("cate") CategoryRequest categoryRequest);
    @Select("""
            UPDATE categories_tb set category_name=#{cate.categoryName} where category_id=#{id} returning  * ;
            """)
    @ResultMap("mapCategory")
    Category updateCategory(@Param("cate") CategoryRequest categoryRequest, Integer id);
}
