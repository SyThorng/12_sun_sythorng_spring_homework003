package com.example._12_pvh_sunsythorng_hw003.repository;

import com.example._12_pvh_sunsythorng_hw003.model.Authors;
import com.example._12_pvh_sunsythorng_hw003.model.Book;
import com.example._12_pvh_sunsythorng_hw003.model.Category;
import com.example._12_pvh_sunsythorng_hw003.model.request.BookRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {

    @Select("""
            select *
            from book_details
            inner join categories_tb ct on ct.category_id = book_details.category_id
            where book_id =#{id}
            """)
    @Result(property = "categoryId", column = "category_id")
    @Result(property = "categoryName", column = "category_name")
    List<Category> getCategoryByBookId(int id);

    @Select("""
            SELECT * FROM books
            """)
    @Results(id = "mapBook", value = {
            @Result(property = "bookId", column = "book_id"),
            @Result(property = "publishedDate", column = "published_date"),
            @Result(property = "authors", column = "author_id", one = @One(select = "com.example._12_pvh_sunsythorng_hw003.repository.AuthorRepository.getAllAuthorById")),
            @Result(property = "categories", column = "book_id", many = @Many(select = "getCategoryByBookId"))
    })
    List<Book> getAllBook();

    @Select("""
            SELECT * from books where book_id=#{id}
             """)
    @ResultMap("mapBook")
    Book searchBookById(Integer id);

    @Select("""
            DELETE  from books where book_id=#{id}
             """)
    @ResultMap("mapBook")
    Book deleteBookById(Integer id);

    @Select("""
            insert into books (title,author_id) values (#{book.title},#{book.authors})
             returning book_id
            """)
    Integer insertBook(@Param("book") BookRequest bookRequest);

    @Insert("""
            insert into book_details(book_id,category_id)values(#{id},#{category_id})
            """)
    void addBookDetails(Integer id, Integer category_id);
    @Select("""
            UPDATE books set title=#{book.title},author_id=#{book.authors} where book_id=#{id} returning book_id
            """)
    Integer updateBook(Integer id,@Param("book") BookRequest bookRequest);

    @Delete("""
            DELETE from book_details where book_id=#{id}
            """)
    void deleteBookforUpdate(Integer id);
}
