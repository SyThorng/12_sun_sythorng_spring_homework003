package com.example._12_pvh_sunsythorng_hw003.repository;

import com.example._12_pvh_sunsythorng_hw003.model.Authors;
import com.example._12_pvh_sunsythorng_hw003.model.request.AuthorsRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AuthorRepository {

    @Select("Select * from authors_tb")
    @Results(id = "mapAuthor", value = {
            @Result(property = "authorId", column = "author_id"),
            @Result(property = "authorName", column = "author_name")
    })
    List<Authors> getAllAuthor();

    @Select("""
            Select * from authors_tb where author_id=#{id}
            """)
    @ResultMap("mapAuthor")
    Authors getAllAuthorById(Integer id);

    @Select("""
            DELETE  FROM authors_tb where author_id=#{id}
             """)
    @ResultMap("mapAuthor")
    Authors deleteAuthor(Integer id);

    @Select("""
            insert into authors_tb (author_name, gender) values (#{author.authorName},#{author.gender}) returning *
            """)
    @ResultMap("mapAuthor")
    Authors insertAuthor(@Param("author") AuthorsRequest authorsRequest);


    @Select("""
            UPDATE authors_tb set author_name=#{author.authorName},gender=#{author.gender} where author_id=#{id} returning *
            """)
    @ResultMap("mapAuthor")
    Authors updateAuthor(@Param("author") AuthorsRequest authorsRequest, Integer id);
}
