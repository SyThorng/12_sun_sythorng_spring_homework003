package com.example._12_pvh_sunsythorng_hw003.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category {
    int categoryId;
    String categoryName;
}
