package com.example._12_pvh_sunsythorng_hw003.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book {

    int bookId;
    private String title;
    Timestamp publishedDate;
    Authors authors;
    List<Category> categories;

}
