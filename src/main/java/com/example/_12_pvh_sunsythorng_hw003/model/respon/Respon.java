package com.example._12_pvh_sunsythorng_hw003.model.respon;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Respon<T> {


    Timestamp timestamp;
    Integer status;
    String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    T payload;
}
