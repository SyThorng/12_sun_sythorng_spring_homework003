package com.example._12_pvh_sunsythorng_hw003.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookRequest {
    private String title;

    Integer authors;
    List<Integer> categories;

}
