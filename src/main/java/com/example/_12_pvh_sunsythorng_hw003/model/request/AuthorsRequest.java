package com.example._12_pvh_sunsythorng_hw003.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorsRequest {
    private String authorName;
    private String gender;
}
